//
//  UIViewController+.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/14.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorDialog(message: String) {
        let alertController = UIAlertController(title: "エラー",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "閉じる", style: .default))
        present(alertController, animated: true)
    }
}
