//
//  BasaResponse.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/06.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
