//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by ykatsume on 2020/07/28.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import UIKit
import Alamofire
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {

    @IBOutlet private weak var todoTableView: UITableView!

    private var todos: [Todo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        setUpNavigationBar()
        fetchTodoList()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] message in
                self?.showErrorDialog(message: message)
            }
        )
    }

    @IBAction private func didTapPencilButton(_sender: Any) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateViewController(withIdentifier: "todoEdit") as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        show(todoEditViewController, sender: nil)
    }

    private func setUpNavigationBar() {
        navigationItem.title = "TODO APP"
        navigationController?.navigationBar.barTintColor = .blue
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todos[indexPath.row]
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidTodoEdit(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}
