//
//  TodoUpdateRequest.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/14.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Alamofire

struct TodoUpdateRequest: RequestProtocol {
    typealias Response = CommonResponse

    let todo: Todo

    var path: String {
        return "/todos/\(todo.id)"
    }

    var method: HTTPMethod {
        return .put
    }

    var parameters: Parameters? {
        var parameters = ["title": todo.title, "detail": todo.detail, "date": nil]
        if let date = todo.date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "JST")
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            parameters["date"] = formatter.string(from: date)
        }
        return parameters as Parameters
    }
}
