//
//  Todo.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/07.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Alamofire

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
