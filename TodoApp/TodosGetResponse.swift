//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/07.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Foundation

struct TodosGetResponse: BaseResponse {
    let todos: [Todo]
    let errorCode: Int
    let errorMessage: String
}
