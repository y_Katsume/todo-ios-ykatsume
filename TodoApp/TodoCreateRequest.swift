//
//  TodoCreateRequest.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/12.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Alamofire

struct TodoCreateRequest: RequestProtocol {
    typealias Response = CommonResponse

    let title: String
    let detail: String?
    let date: Date?

    var path: String {
        return "/todos"
    }

    var method: HTTPMethod {
        return .post
    }

    var parameters: Parameters? {
        var parameters = ["title": title]
        if let detail = detail {
            parameters["detail"] = detail
        }
        if let date = date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "JST")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
