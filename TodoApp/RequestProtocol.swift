//
//  RequestProtocol.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/07.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Alamofire

protocol RequestProtocol {
    
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        return "https://todoapp00000000000000001.herokuapp.com"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
