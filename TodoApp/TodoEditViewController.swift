//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by ykatsume on 2020/07/31.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import UIKit
import Alamofire

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidTodoEdit(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {

    weak var delegate: TodoEditViewControllerDelegate?
    var todo: Todo?
    private let titleMaxCount = 100
    private let detailMaxCount = 1000
    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()

    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var registerButton: UIButton!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var titleCountLabel : UILabel!

    private var titleText: String {
        titleTextField.text ?? ""
    }

    private var detailText: String {
        detailTextView.text
    }

    private var isTitleOverCount: Bool {
        titleText.count > titleMaxCount
    }

    private var isDetailOverCount: Bool {
        detailText.count > detailMaxCount
    }

    private var canActiveRegisterButton: Bool {
        !titleText.isEmpty && !isTitleOverCount && !isDetailOverCount
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        setUpViews()
        switchRegisterButtonState()
        setUpDatePicker()
        dateFormatter.dateFormat = "yyyy/M/d"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        setUpRegisterButtonTitle()
        if let todo = todo {
            setText(todo: todo)
            setUpTextCount()
        }
    }

    @IBAction private func onChangeTitleText(_ sender: Any) {
        titleCountLabel.text = String(titleText.count)
        titleCountLabel.textColor = isTitleOverCount ? .red : .black
        switchRegisterButtonState()
    }

    @IBAction private func onClickRegisterButton(_ sender: Any) {
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        let detail = detailText.isEmpty ? nil : detailText
        if let id = todo?.id {
            updateTodo(id: id, title: titleText, detail: detail, date: date)
        } else {
            registerTodo(title: titleText, detail: detail, date: date)
        }
    }

    private func registerTodo(title: String, detail: String?, date: Date?) {
        APIClient().call(
            request: TodoCreateRequest(title: title, detail: detail, date: date),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidTodoEdit(self, message: "登録しました")
            }, failure: { [weak self] message in
                self?.showErrorDialog(message: message)
            }
        )
    }

    private func updateTodo(id: Int, title: String, detail: String?, date: Date?) {
        let todo = Todo(id: id, title: title, detail: detail, date: date)
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidTodoEdit(self, message: "更新しました")
            },
            failure: { [weak self] message in
                self?.showErrorDialog(message: message)
            }
        )
    }

    private func setText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    private func setUpTextCount() {
        if let titleTextCount = titleTextField.text?.count {
            titleCountLabel.text = String(titleTextCount)
        }
        detailCountLabel.text = String(detailTextView.text.count)
    }

    private func disableRegisterButton() {
        registerButton.isEnabled = false
        registerButton.layer.backgroundColor = UIColor.gray.cgColor
    }

    private func enableRegisterButton() {
        registerButton.isEnabled = true
        registerButton.layer.backgroundColor = UIColor.blue.cgColor
    }

    private func setUpRegisterButtonTitle() {
        let title = todo?.id == nil ? "登録" : "更新"
        registerButton.setTitle(title, for: .normal)
    }

    private func switchRegisterButtonState() {
        if canActiveRegisterButton {
            enableRegisterButton()
        } else {
            disableRegisterButton()
        }
    }

    private func setUpNavigationBar() {
        navigationItem.title = "TODO APP"
    }

    private func setUpViews() {
        detailTextView.layer.borderColor = UIColor.black.cgColor
        detailTextView.layer.borderWidth = 1.0
        detailTextView.layer.cornerRadius = 5.0
        detailTextView.layer.masksToBounds = true
        titleTextField.layer.borderColor = UIColor.black.cgColor
        titleTextField.layer.borderWidth = 1.0
        titleTextField.layer.cornerRadius = 5.0
        dateTextField.layer.borderColor = UIColor.black.cgColor
        dateTextField.layer.borderWidth = 1.0
        dateTextField.layer.cornerRadius = 5.0
        detailTextView.delegate = self
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        dateTextField.inputView = datePicker
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let decisionBarButtonItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(onTappedDecisionBarButton))
        let deleteBarButtonItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(onTappedDeleteBarButton))
        let closeBarButtonItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(onTappedCloseBarButton))
        toolbar.setItems([deleteBarButtonItem, spaceItem, closeBarButtonItem, decisionBarButtonItem], animated: true)

        dateTextField.inputAccessoryView = toolbar
    }

    @objc private func onTappedDecisionBarButton() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc private func onTappedDeleteBarButton() {
        dateTextField.text = ""
        dateTextField.endEditing(true)
    }

    @objc private func onTappedCloseBarButton() {
        dateTextField.endEditing(true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let detailTextCount = detailText.count
        detailCountLabel.text = String(detailTextCount)
        detailCountLabel.textColor = isDetailOverCount ? .red : .black
        switchRegisterButtonState()
    }
}
