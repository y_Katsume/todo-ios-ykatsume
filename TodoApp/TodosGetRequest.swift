//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by ykatsume on 2020/08/07.
//  Copyright © 2020 ykatsume. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var parameters: Parameters? {
        return nil
    }

    var path: String {
        return "/todos"
    }

    var method: HTTPMethod {
        return .get
    }
}
